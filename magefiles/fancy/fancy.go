package fancy

import (
	"github.com/pterm/pterm"
	"github.com/sheldonhull/magetools/ci"
	"github.com/sheldonhull/magetools/pkg/magetoolsutils"
)

const ptermMargin = 10

func IntroScreen(disableStyling bool) {
	magetoolsutils.CheckPtermDebug()
	if disableStyling || ci.IsCI() {
		pterm.DisableStyling()
	}
	ptermLogo, _ := pterm.DefaultBigText.WithLetters(
		pterm.NewLettersFromStringWithStyle("Test", pterm.NewStyle(pterm.FgLightBlue)),
	).Srender()

	pterm.DefaultCenter.Print(ptermLogo)

	pterm.DefaultCenter.Print(pterm.DefaultHeader.WithFullWidth().
		WithBackgroundStyle(pterm.NewStyle(pterm.BgLightBlue)).
		WithMargin(ptermMargin).Sprint("Task Automation With Go"))

	pterm.Println()
}
