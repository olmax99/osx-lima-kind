package gokind

import (
	"os/user"

	"github.com/magefile/mage/sh"
	"github.com/pterm/pterm"
	"github.com/spf13/viper"
)

type viConfig struct {
	Kind struct {
		Name   string `yaml:"name"`
		Setup  string `yaml:"setup"`
		Extnet string `yaml:"extnet"`
	} `yaml:"kind"`
	Services []struct {
		Name string `yaml:"name"`
		Root string `yaml:"root"`
	} `yaml:"services"`
	Registry struct {
		Server   string `yaml:"server"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Email    string `yaml:"email"`
	} `yaml:"registry"`

	Workspace string `yaml:"workspace"`
	Gitroot   string `yaml:"gitroot"`
	User      string `yaml:"user"`
}

func NewViConfig() *viConfig {
	return &viConfig{}
}

func (c *viConfig) conf() error {
	// set git root dir
	r, err := sh.Output(
		"git",
		"rev-parse",
		"--show-toplevel",
	)
	if err != nil {
		pterm.Info.Printfln("gitroot not found. %s", err.Error())
	}
	w, err := sh.Output("basename", []string{
		r,
	}...)
	if err != nil {
		pterm.Info.Printfln("gitroot not found. %s", err.Error())
	}

	viper.SetDefault("gitroot", r)
	viper.SetDefault("workspace", w)
	viper.SetDefault("user", CurrentUser().Name)

	viper.SetConfigName("kind")
	viper.AddConfigPath("magefiles/mage/.config")
	viper.SetConfigType("yml")

	if err := viper.ReadInConfig(); err == nil {
		pterm.Info.Printf("Using config file: %s\n", viper.ConfigFileUsed())

	} else {
		pterm.Error.Printf("ReadConfig: %s", err)
	}

	if err := viper.Unmarshal(&c); err != nil {
		pterm.Error.Println("Unmarshalling failed.")
		return err
	}

	return nil
}

func CurrentUser() *user.User {
	u, err := user.Current()
	if err != nil {
		panic(err)
	}
	return u
}
