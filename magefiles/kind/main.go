package gokind

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
	"text/template"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"github.com/pterm/pterm"

	mtu "github.com/sheldonhull/magetools/pkg/magetoolsutils"
)

type Kind mg.Namespace

var (
	vic *viConfig
)

func init() {
	vic = NewViConfig()
}

/*
Start your local workspace with KinD
*/
func (Kind) Build() error {
	vic.conf()
	mg.SerialDeps(
		mg.F(Kind.Cluster),
		mg.F(Kind.Kube),
	)
	return nil
}

// Create local KinD
func (Kind) Cluster() error {
	pterm.Info.Println("Create local KinD from dev/kind-config.yml...")
	MustRunV(
		"kind",
		"create",
		"cluster",
		"--name", vic.Kind.Name,
		"--config", "dev/kind-config.yml",
	)
	return nil
}

/*
This task is simply calling your solution script
*/
func (Kind) Lima() error {
	vic.conf()
	cmd, err := exec.Command("/bin/bash", vic.Gitroot+"/script/build_lima.sh").Output()
	if err != nil {
		return err
	}
	output := string(cmd)
	pterm.Info.Println(output)
	return nil
}

/*
Prepare cluster
*/
func (Kind) Kube() error {
	mg.SerialDeps(
		mg.F(kubeCtlApply, "./dev/manifests/metrics-server/deploy.yml", "kube-system"),
	)

	switch s := vic.Kind.Setup; {
	case s == "minimal":
		pterm.Info.Printfln("%s: Install Monitoring only...", vic.Kind.Setup)
	case s != "minimal":
		pterm.Info.Printfln("%s: Build and install MetalLB, Monitoring, Istio, and Ambassador Ingress...", vic.Kind.Setup)
	}
	mg.SerialDeps(
		mg.F(day2Setup),
	)
	return nil
}

/*
Remove your local kind cluster
*/
func (Kind) Teardown() error {
	vic.conf()

	pterm.Info.Println("Delete local KinD local-dev...")
	MustRunV(
		"kind",
		"delete",
		"cluster",
		"--name", vic.Kind.Name,
	)
	return nil
}

func day2Setup() error {
	mg.Deps(
		mg.F(helmAddRepo, "datawire", "https://app.getambassador.io"),
		mg.F(helmAddRepo, "prometheus", "https://prometheus-community.github.io/helm-charts"),
		mg.F(kubeCtlCreateNs, "emissary-system"),
		mg.F(kubeCtlCreateNs, "emissary"),
		mg.F(kubeCtlCreateNs, "test-http-ambassador"),
		mg.F(kubeCtlCreateNs, "metallb-system"),
	)
	MustRunV("helm", "repo", "update")
	switch s := vic.Kind.Setup; {
	case s == "minimal":
	case s != "minimal":
		mg.Deps(
			mg.F(kubeAmbassadorCRs, "3.2.0", "emissary-system"),
			mg.F(kubeMetallbCRs, "0.13.7", "metallb-system"),
		)
		mg.SerialDeps(
			mg.F(metallbInstall,
				"dev/manifests/metallb-",
				"0.13.7",
				"metallb-system",
				vic.Kind.Extnet,
			),
		)

		mg.Deps(
			mg.F(ambassadorIngressInstall,
				"dev/manifests/ambassador-emissary-",
				"3.2.0",
				"emissary",
			),
		)
	}

	pterm.Success.Println("Deploy Sample App.")
	kubeCtlApply(strings.Join([]string{
		vic.Gitroot, "dev/manifests/sample-app",
	}, "/"), "test-http-ambassador")

	return nil
}

// TODO could be moved to global mage tasks
func resolvKindCli() error {
	mtu.CheckPtermDebug()

	pterm.Debug.Printfln("resolveKindcli")
	binary, err := exec.LookPath("kind")
	if err != nil {
		pterm.Debug.Printfln("exec.LookPath(\"kind\") failed")
		return err
	}
	pterm.Debug.Printfln("found", binary)

	return nil
}

func metallbInstall(p, v, ns, iprange string) error {
	// setup address pool used by loadbalancers
	net, err := sh.Output(
		"docker",
		"network",
		"inspect", "-f", "'{{.IPAM.Config}}'",
		"kind",
		"--format", "'{{json . }}'",
	)
	if err != nil {
		return err
	}
	pterm.Debug.Printfln("docker net:", net)
	if err := writeKubeTemplate(p, v, iprange); err != nil {
		return err
	}

	kubeCtlApply(p+v+"/config/metallb.yml", ns)

	pterm.Success.Println("MetalLB ready.")
	return nil
}

func ambassadorIngressInstall(p, v, ns string) error {
	MustRunV(
		"helm",
		"install",
		"emissary-ingress",
		"--namespace", ns,
		// "-f", vic.Gitroot+"/"+p+v+"/istio-integration.yml",
		"datawire/emissary-ingress",
		"--version", "8.2.0",
	)
	MustRunV(
		"kubectl",
		"wait",
		"-n", ns,
		"--timeout=180s",
		"--for=condition=available",
		"deploy",
		"-l", "app.kubernetes.io/instance=emissary-ingress",
	)
	pterm.Success.Println("Ambassador emissary-ingress ready.")
	return nil
}

// TODO could be part of global docker task
func dockerNetworkInpect() error {
	MustRunV(
		"docker",
		"network",
		"inspect", "-f", "'{{.IPAM.Config}}'",
		"kind",
		"--format", "'{{json . }}'",
	)

	return nil
}

// "emissary-system", "3.2.0"
func kubeAmbassadorCRs(ctx context.Context, v, ns string) error {
	kubeCtlApply(
		strings.Join([]string{
			vic.Gitroot, "dev/manifests/ambassador-emissary-" + v, "aes.yml",
		}, "/"),
		ns,
	)

	MustRunV(
		"kubectl",
		"wait",
		"-n", ns,
		"--timeout=90s",
		"--for=condition=available",
		"deployment", "emissary-apiext",
	)
	return nil
}

// "metallb-system", "0.13.7"
func kubeMetallbCRs(ctx context.Context, v, ns string) error {
	kubeCtlApply(
		strings.Join([]string{
			vic.Gitroot, "dev/manifests/metallb-" + v, "config", "native.yml",
		}, "/"),
		ns,
	)

	MustRunV(
		"kubectl",
		"wait",
		"-n", ns,
		"--timeout=90s",
		"--for=condition=ready",
		"pod", "--selector=app=metallb",
	)
	return nil
}

func kubeCtlApply(f, ns string) error {
	MustRunV(
		"kubectl",
		"apply",
		"-f", f,
		"-n", ns,
	)
	return nil
}

func kubeCtlCreateNs(ns string) error {
	MustRunV(
		"kubectl",
		"create",
		"namespace", ns,
	)
	return nil
}

func helmAddRepo(name, address string) error {
	MustRunV(
		"helm",
		"repo",
		"add",
		name,
		address,
	)
	return nil
}

func writeKubeTemplate(p, v, iprange string) error {
	pterm.Info.Printfln("[lxc] Writing metallb template in '%s'..", p+v)

	data := make(map[string]interface{}, 1)
	data["name"] = "example"
	data["namespace"] = "metallb-system"
	data["iprange"] = iprange
	b := textTemplate(
		strings.Join([]string{
			vic.Gitroot, p + v, "config", "metallb.tpl",
		}, "/"),
		"metallb.tpl",
		data)
	err := ioutil.WriteFile(
		strings.Join([]string{
			vic.Gitroot, p + v, "config", "metallb.yml",
		}, "/"),
		b.Bytes(),
		0755,
	)
	if err != nil {
		fmt.Printf("Unable to write file: %v", err)
	}

	return nil
}

func textTemplate(f, t string, b map[string]interface{}) *bytes.Buffer {
	tmpl := template.Must(template.New(t).Delims("[[", "]]").ParseFiles(f))
	buf := &bytes.Buffer{}
	err := tmpl.Execute(buf, b)
	if err != nil {
		panic(err)
	}
	return buf
}

func MustRunV(cmd string, args ...string) {
	sh.RunV(cmd, args...)
}
