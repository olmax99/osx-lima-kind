####### AUTO CREATED BY MAGE #######
# BEWARE THAT CHANGES MAY GET LOST #
####################################
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: [[.name]]
  namespace: [[.namespace]]
spec:
  addresses:
  - [[.iprange]]
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: empty
  namespace: [[.namespace]]
